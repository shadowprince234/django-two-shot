from django.contrib import admin
from receipts.models import Receipt, ExpenseCategory, Account
# Register your models here.

@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    User = (
        "name",
        "user",
    )


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    User = (
        "name",
        "number",
        "owner",
    )


@admin.register(Receipt)
class RecieptAdmin(admin.ModelAdmin):
    User = (
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    )
